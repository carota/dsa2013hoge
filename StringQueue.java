
public class StringQueue
{
	private int max;
	private int front;
	private int rear;
	private int num;
	private String[] que;

	public class EmptyStringQueueException extends RuntimeException
	{
		public EmptyStringQueueException(){}
	}

	public class OverflowStringQueueException extends RuntimeException
	{
		public OverflowStringQueueException(){}
	}

	public StringQueue(int capacity)
	{
		num = front = rear = 0;
		max = capacity;

		try{
			que = new String[max];
		}catch(OutOfMemoryError e){
			max = 0;
		}
	}

	public String enque(String x) throws OverflowStringQueueException
	{
		if(num >= max)
			throw new OverflowStringQueueException();
		que[rear++] = x;
		num++;

		if(rear == max)
			rear = 0;
		return x;
	}

	public String deque() throws EmptyStringQueueException
	{
		if(num <= 0)
			throw new EmptyStringQueueException();
		String x = que[front++];
		num--;

		if(front == max)
			front = 0;
		return x;
	}

	public String peek() throws EmptyStringQueueException
	{
		if(num <= 0)
			throw new EmptyStringQueueException();
		return que[front];
	}

	public int indexOf(String x)
	{
		for(int i=0; i<num; i++)
		{
			int idx = (i + front) % max;
			if(que[idx] == x)
				return idx;
		}
		return -1;
	}

	public void clear()
	{
		num = front = rear = 0;
	}

	public int capacity()
	{
		return max;
	}

	public int size()
	{
		return num;
	}

	public boolean isEmpty()
	{
		return num <=0;
	}

	public boolean isFull()
	{
		return num >=0;
	}

	public void dump()
	{
		if(num <= 0)
			System.out.println("キューは空です。");
		else{
			for(int i=0; i<max; i++)
			{
				//↓ここをque[(i + front) % max]ではなく、このようにする
				System.out.print(i +":"+ que[i % max] +" ");

				if(i == front)
					System.out.print("(front)");
				else if(i == rear)
					System.out.print("(rear)");
				System.out.println();
			}
			System.out.println();
		}
	}
}