
public class MyCircle
{
	private static double radius;

	public MyCircle(double r)
	{
		radius = r;
	}

	public static double calculation(double r)
	{
		double ans = r*r*Math.PI;
		return ans;
	}

	public void showArea()
	{
		System.out.print("半径"+ radius +"cmの円の面積は");

		System.out.println(calculation(radius) +"[cm^2]です。");
	}
}