
public class TestAnalyzer
{
	public static boolean isDigits(String str)
	{
		boolean judge = true;

		for(int i=0; i<str.length(); i++)
		{
			char c = str.charAt(i);

			//０～９でないときはfalseを返すようにしてゴリ押しした。
			if(c!='0' && c!='1' && c!='2' && c!='3' && c!='4' && c!='5' && c!='6' && c!='7' && c!='8' && c!='9')
				return judge = false;
		}
		return judge;
	}

	public static void main(String[] args)
	{
		String target = "Today is May 20 2013";

		String[] tokens = target.split(" ");
		int tokenNum = tokens.length;

		System.out.println("target = "+ target);
		System.out.println("tokenNum = "+ tokenNum);

		for(int i=0; i<tokenNum; i++)
		{
			System.out.print(i +":" + tokens[i]);

			if(isDigits(tokens[i]))
				System.out.println(" (digit)");
			else
				System.out.println();
		}
	}
}
