
public class MyRectangle
{
	private static double width;
	private static double height;

	public MyRectangle(double h, double w)
	{
		width = w;
		height = h;
	}

	public static double calculation(double w,double h)
	{
		double ans = w*h;
		return ans;
	}

	public void showArea()
	{
		System.out.print("高さ"+ height +"cm、");
		System.out.print("幅"+ width +"cmの長方形の面積は");
		System.out.println(calculation(width, height) +"[cm^2]です。");
	}
}
