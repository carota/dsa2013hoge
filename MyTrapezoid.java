
public class MyTrapezoid
{
	private static double base1, base2;
	private static double height;

	public MyTrapezoid(double b1, double b2, double h)
	{
		base1= b1;
		base2 =b2;
		height = h;
	}

	public static double calculation(double b1,double b2,double h)
	{
		double ans = (b1+b2)*h/2;
		return ans;
	}

	public void showArea()
	{
		System.out.print("上辺"+ base1 +"cm、");
		System.out.print("低辺"+ base2 +"cm、");
		System.out.print("高さ"+ height +"cmの台形の面積は");
		System.out.println(calculation(base1, base2, height) +"[cm^2]です。");
	}
}