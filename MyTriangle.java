
public class MyTriangle
{
	private static double base;
	private static double height;

	public MyTriangle(double b, double h)
	{
		base = b;
		height = h;
	}

	public static double calculation(double b,double h)
	{
		double ans = b*h/2;
		return ans;
	}

	public void showArea()
	{
		System.out.print("底辺"+ base +"cm、");
		System.out.print("高さ"+ height +"cmの三角形の面積は");
		System.out.println(calculation(base, height) +"[cm^2]です。");
	}
}